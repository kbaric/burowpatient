﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BurowPatient.Models
{
    public static class BasicData
    {
        public static SelectList Spolovi
        {
            get {
                return CreateListSpolovi();
            }
        }

        public static SelectList CreateListSpolovi()
        {
            List<Spol> sp = new List<Spol>();
            Spol spol = new Spol() { Gender = 0, Name = "M (M)" };
            sp.Add(spol);
            spol = new Spol() { Gender = 1, Name = "F (Ž)" };
            sp.Add(spol);

            return new SelectList(sp, "Gender", "Name");
        }
    }
}