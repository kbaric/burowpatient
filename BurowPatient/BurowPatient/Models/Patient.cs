﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BurowPatient.Models
{
    public class Patient
    {
        [Required]
        [Display(Name = "First name (Ime)")]
        public string FName { get; set; }

        [Required]
        [Display(Name = "Last name (Prezime)")]
        public string LName { get; set; }

        [Display(Name = "Title (Titula)")]
        public string Title { get; set; }

        [Display(Name = "Birth Date (Datum rođenja)")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yy}")]
        public DateTime BirthDate { get; set; }

        [Display(Name = "Mobile Phone (Mobitel)")]
        public string WirelessPhone { get; set; }

        [Display(Name = "Work Phone (Poslovni telefon)")]
        public string WkPhone { get; set; }

        [Display(Name = "Email (Email)")]
        public string Email { get; set; }

        [Display(Name = "Address (Adresa)")]
        public string Address { get; set; }

        [Display(Name = "City (Grad)")]
        public string City { get; set; }

        [Display(Name = "State (Država)")]
        public string State { get; set; }

        [Display(Name = "Zip (Broj pošte)")]
        public string Zip { get; set; }

        [Display(Name = "Gender (Spol)")]
        public int Gender { get; set; }
    }

}
