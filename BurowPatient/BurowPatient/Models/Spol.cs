﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BurowPatient.Models
{
    public class Spol
    {
        public int Gender { get; set; }
        public string Name { get; set; }
    }
}