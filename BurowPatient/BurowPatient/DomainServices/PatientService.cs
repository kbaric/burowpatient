﻿using BurowPatient.Models;
using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace BurowPatient.DomainServices
{
    public class PatientService
    {
        internal bool Insert(Patient patient)
        {
            MySqlConnection conn;
            var myConnectionString = Properties.Settings.Default.ConnectionString;

            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                string sqlCommand = "INSERT INTO patient(LName, FName,Title,BirthDate,WirelessPhone,WkPhone, Email,Address,City, State,Zip, Gender) VALUES('LNameValue','FNameValue','TitleValue','BirthDateValue','WirelessPhoneValue','WkPhoneValue', 'EmailValue','AddressValue','CityValue', 'StateValue','ZipValue', 'GenderValue')";

                sqlCommand= sqlCommand.Replace("LNameValue", patient.LName);
                sqlCommand = sqlCommand.Replace("FNameValue", patient.FName);
                sqlCommand = sqlCommand.Replace("TitleValue", patient.Title);
                sqlCommand = sqlCommand.Replace("BirthDateValue", patient.BirthDate.ToString("yyyy-MM-dd"));
                sqlCommand = sqlCommand.Replace("WirelessPhoneValue", patient.WirelessPhone);
                sqlCommand = sqlCommand.Replace("WkPhoneValue", patient.WkPhone);
                sqlCommand = sqlCommand.Replace("EmailValue", patient.Email);
                sqlCommand = sqlCommand.Replace("AddressValue", patient.Address);
                sqlCommand = sqlCommand.Replace("CityValue", patient.City);
                sqlCommand = sqlCommand.Replace("StateValue", patient.State);
                sqlCommand = sqlCommand.Replace("ZipValue", patient.Zip);
                sqlCommand = sqlCommand.Replace("GenderValue", patient.Gender.ToString());

                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sqlCommand;
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.BeginExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return true;
        }
    }
}