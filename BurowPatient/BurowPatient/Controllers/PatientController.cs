﻿using BurowPatient.DomainServices;
using BurowPatient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BurowPatient.Helpers;

namespace BurowPatient.Controllers
{
    public class PatientController : Controller
    {
        private readonly PatientService _service = new PatientService();

        // GET: Patient
        public ActionResult Index()
        {
            MyGlobalVariables.AlreadySaved = true;
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.SpoloviList = BasicData.Spolovi;
            MyGlobalVariables.AlreadySaved = false;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FName,LName,Title,BirthDate,WirelessPhone,WkPhone, Email,Address,City, State,Zip, Gender")] Patient patient)
        {
            if (MyGlobalVariables.AlreadySaved == true)
            {
                return RedirectToAction("Index");
            }

            ViewBag.SpoloviList = BasicData.Spolovi;

            if (ModelState.IsValid)
            {
                try
                {
                    _service.Insert(patient);
                    MyGlobalVariables.AlreadySaved = true;
                    return RedirectToAction("Index");
                }
                catch (System.Exception ex)
                {
                    ModelState.AddModelError("", "Error: " + ex.InnerException.InnerException.ToString());
                }
            }

            return View(patient);
        }
    }
}